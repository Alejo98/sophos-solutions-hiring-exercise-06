# Code Challenge 06
![Colpatria logo](https://scotiabankfiles.azureedge.net/scotiabank-colombia/Global-Rebrand/logo-site-2.png)

## Disclaimer
*By reading this problem statement, I agree not to copy code from any source (including websites, books, or friends and colleagues) to complete this assessment. I also agree not to share this code challenge/assessment through any means. I may, however, reference programming language documentation or use an IDE that has code completion features.*


## Code Challenge

### Risk Assessment
Some statistics and data points are required to evaluate the risk of defaulting by potential new customers; these are the standard deviation of past late payments, unusual late payment, and probability of late payments.

The unusual late payment is detected when there is a very late payment *(number of days after the payment due date)*, and the number of days of late payments _before_ and _after_ this day is **smaller**; graphically, you would see a peak. For this data, we require the largest peak found *(max peak)*.

To calculate the probability of late payments, we must use the late payments of other products the client already has, given a number of time periods.

#### Input

For both the standard deviation and unusual late payment *(max peak)*, you will receive an integer array containing the number of days of delays in payments.

Each index of the array represents a payment period, usually a month, `0` meaning no delay, `1` meaning one day of delay in payment, and so on.

To calculate the late payment probability, you will receive a two dimensional array, where every row represents the late payments of a product for different time periods *(columns)*.

#### Expected output

##### Expected output for Standard deviation

The expected output for the standard deviation is a `double`, representing the result of calculating the standard deviation (*The population standard deviation*).

##### Expected output for unusual late payment *(max peak)*

The expected output is an `int` representing the index of the array where the *max peak* is found.

##### Expected output for late payment probability

The expected output is a `double` array where each position contains the late payment probability for the period *(column index)* of the different products *(rows of the array)*.

#### Example

##### Sample input
```
payment delays array:
{0, 15, 2, 0, 1, 3}
[0  1   2  3  4  5] //index
```
##### Sample input for late payment probability

```
A two-dimensional array, each row is a product, each column is a time period.
{
	{0, 3, 6, 1, 0, 5, 0, 0}, //product 1
	{0, 3, 0, 2, 0, 0, 0, 2}, //product 2
	{0, 0, 1, 0, 3, 0, 2, 0}, //product 3
	{0, 4, 0, 2, 0, 1, 1, 0}  //product 4
	[0  1  2  3  4  5  6  7]  //index (time period)
}
```

##### Sample output
##### Standard deviation
```
5.251983752
```
##### Unusual late payment *(max peak)*
```
1 //index of the array where the max peak is located.
```

##### Late payment probability
```
{0.0, 0.75, 0.5, 0.75, 0.25, 0.5, 0.5, 0.25}
```
##### Explanation
The standard deviation for these numbers `{0, 15, 2, 0, 1, 3}` is `5.251983752`.

The *max peak* for the array is located at index `1` since `15 > 0 and 15 > 2` index 5 also has a peak but `3` is smaller than `15`.

Let see the first two periods *(indices 0 and 1 of the two-dimensional array)* for the late payment probability.
For the first period *(index 0)*, we see no late payments in any of the `4` products, so the result is `0.0` `0/4 = 0.0`.
For the second period *(index 1)*, we see three late payments *(3, 3, 4)*; this gives us a result of `0.75` `3/4 = 0.75`.

#### Completing the code challenge

To complete the code challenge, you must complete the methods `standardDeviation`, `paymentDelayMaxPeakIndex` and `latePaymentProbabilityByPeriod` inside the `CreditRiskAssessment` class so that every unit test in `CreditRiskAssessmentTest` passes.

*Bonus: Document the source code to get bonus points.*

Good luck!
=======
# Sophos-solutions-hiring-exercise-06



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Alejo98/sophos-solutions-hiring-exercise-06.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Alejo98/sophos-solutions-hiring-exercise-06/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> c408227978c6bdd30bf29c32fe9b9d65a1fab798
