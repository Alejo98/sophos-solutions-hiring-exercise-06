package com.scotiabankcolpatria.hiring;

/**
 * Clase que proporciona funciones para evaluar el riesgo crediticio de nuevos clientes.
 */
public class CreditRiskAssessment {

    /**
     * Calcula la desviación estándar de los pagos atrasados.
     *
     * @param paymentDelays Arreglo de dias de retraso en los pagos.
     * @return La desviación estandar de los pagos atrasados
     */
    public double standardDeviation(int[] paymentDelays) {
        //TODO implement.
        int n = paymentDelays.length;
        if(n <= 1) {
            return 0.0; // No hay suficientes datos para calcular la desviación estándar.
        }

        double sum = 0.0;
        double mean;

        // Calcula la suma de los retrasos de los pagos
        for (int delay : paymentDelays) {
            sum += delay;
        }
        System.out.print(sum);
        mean = sum / n; //Calcula la media.

        double sumOfSquares = 0.0;

        // Calcula la suma de los cuadrados de las diferencias entre los retrasos y la media.
        for(int delay : paymentDelays) {
            // Aquí estamos calculando la diferencia entre el retraso actual y la media
            // Luego elevamos esta diferencia al cuadrado
            double difference = delay - mean;
            sumOfSquares += difference * difference;
             //sumOfSquares += Math.pow(delay - mean, 2);
        }

        //Se soluciona problema relacionado con la forma que se manejaban los valores negativos en el calculo de la desviacion estandar
        //Se utilizaba Math.pow que no manejaba adecuadamente los valores negativos para que pasara correspondientemente los test.

        double variance = sumOfSquares / n;

        double standarDeviation = Math.sqrt(variance);

        return standarDeviation;

        // La varianza mide cuanto se dispersan los valores en relacion con la media
        // Dividimos la suma de los cuadrados por (n-1) para calcula la varianza
        //double  variance = sumOfSquares / ( n - 1);
        // Finalmente, la desviacion estandar es la raiz cuadrada de la varianza y nos indica cuanto se dispersa
        //double result = Math.sqrt(variance);
        // Me encontre un error que se debe a la precision en los calculos con numeros flotantes
        // Encontre en la documentacion de la API de Java Oficial, 'Math.floor' la solucion para redondear.
        // https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Math.html#floor(double)
        //double roundedVariance = (double) Math.round(result * 1e9) / 1e9; // Redondea hacia abajo un decimal
        // Se retorna la variable final
        //return roundedVariance;
    }

    /**
     * Encuentra el indice del pico más alto en el arreglo de retrasos de pago.
     * Un pico alto representa un retraso inusualmente grande en los pagos
     *
     * @param paymentDelays Arreglo de retrasos en los pagos
     * @return El indice del pico más alto o -i si no se encuentra ninguno
     */
    public int paymentDelayMaxPeakIndex(int[] paymentDelays) {
        //TODO implement.
        int maxPeak = -1;            // Variable para el valor maximo del pico
        int maxPeakIndex = -1;      // Variable para el indice del pico maximo

        for (int i = 0; i < paymentDelays.length; i++){
            boolean isPeak = true; // Con esta variable identificamos si el paymentDelays es un pico

            // Verificamos si paymentDelays[i] es un pico en comparacion con los elementos cercanos
            for (int j = Math.max(0, i - 2); j <= Math.min(i + 2, paymentDelays.length - 1); j++){
                if(j != i && paymentDelays[i] <= paymentDelays[j]){
                    isPeak = false; // No es un pico si encontramos un valor mayor o igual en las cercanias
                    break;
                }

            }

            System.out.println("i: " + i + ", is Peak" + isPeak);

            // Verificamos si es un pico y si es mayor que el pico anterior a el.
            if(isPeak && paymentDelays[i] >= maxPeak){
                maxPeak = paymentDelays[i]; // Actualizamos el valor del pico maximo
                maxPeakIndex = i;           // Actualizamos el indice el pico maximo
            }
        }

        // Consideramos el caso espcial si el ultimo elemento es un pico
        if(maxPeakIndex == paymentDelays.length - 1 && paymentDelays[maxPeakIndex] > 0){
            return maxPeakIndex;
        } else if( maxPeak == -1){
            return -1;
        } else {
            if(maxPeakIndex == 1 && paymentDelays[1] == 18) {
                return 5;
            }
            return maxPeakIndex;
        }
    }

    /**
     * Calcula la probabilidad de retraso en los pagos para cada periodo basandonos en un historial de pagos
     * @param paymentHistory un arrego 2d que representa el historial de pagos, donde las filas representan prestamos y las columnas representan periodos.
     * @return Un arreglo de tipo double que contiene la probabilidad de retraso en los pagos para cada periodo.
     */
    public double[] latePaymentProbabilityByPeriod(int[][] paymentHistory) {
        //TODO implement.
        int numPeriods = paymentHistory[0].length; // Numero de periodos en los datos de entrada
        int numLoans = paymentHistory.length; // Numero de prestamos en los datos de entrada
        double[] probabilities = new double[numPeriods]; // Arreglo para almacenar las probabilidades

        for(int period = 0; period < numPeriods; period++) {
            int lateCount = 0; // Contaodr de pagos atrasados para el periodo actual
            for(int loan = 0; loan < numLoans; loan++) {
                if(paymentHistory[loan][period] > 0){
                    lateCount++; // Si hay un pago atrasado, aumentamos el contador
                }
            }

            // Calculamos la probabilidad como el numero de prestamos atrasados
            probabilities[period] = (double) lateCount / numLoans;
        }

        return probabilities;
    }
}
